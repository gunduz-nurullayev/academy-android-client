package org.shaheen.nazarov.android.sourcecodeacademy;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.shaheen.nazarov.android.sourcecodeacademy.client.HttpClient;
import org.shaheen.nazarov.android.sourcecodeacademy.client.model.UserModel;
import org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2.AuthTokenInfo;
import org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2.Http;
import org.shaheen.nazarov.android.sourcecodeacademy.tool.Constants;
import org.shaheen.nazarov.android.sourcecodeacademy.tool.Regex;

import java.io.IOException;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGN_UP = 0;
    private final Context context = this;
    private HttpClient client = HttpClient.getInstance();

    EditText txt_email;
    EditText txt_password;
    Button btn_login;
    Button btn_forgot;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_login);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_forgot = (Button) findViewById(R.id.btn_forgot);
        if (!client.isNetworkAvailable((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
            showMessage("Check your Internet Connection.");
            btn_login.setText("Check Internet");
            btn_forgot.setEnabled(false);
        }
        txt_email = (EditText) findViewById(R.id.login_email);
        txt_password = (EditText) findViewById(R.id.login_password);
        txt_email.setText(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(Constants.username, ""));
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnLoginAction(view);
            }
        });

    }

    private void btnLoginAction(View view) {
        if (btn_login.getText().length() > 7) {
            if (client.isNetworkAvailable((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
                btn_login.setText("Login");
                btn_forgot.setEnabled(true);
            }
            return;
        }
        if (!validateForm()) {
            showMessage("Login failed");
            return;
        }
        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this, R.style.AppThemeDarkDialog);
        progressDialog.setMessage("Authentication...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        new android.os.Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            preferences.edit()
                                    .putString(Constants.username, txt_email.getText().toString())
                                    .putString(Constants.password, txt_password.getText().toString())
                                    .commit();
                            HttpClient client = HttpClient.getInstance().sharedPreferences(preferences);
                            UserModel userModel = client.getRequest(Http.USER + Http.INFO, UserModel.class);
                            if (userModel != null) {
                                Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
                                preferences.edit()
                                        .putBoolean(Constants.auth, true)
                                        .commit();
                                showMessage("Welcome " + userModel.getMail());;
                                startActivity(intent);
                                finish();
                            }else {
                                preferences.edit()
                                        .remove(Constants.username)
                                        .remove(Constants.password)
                                        .remove(Constants.auth)
                                        .commit();
                                showMessage("Login failed");
                            }
                        } catch (Exception e) {
                            showMessage("Login failed");
                            e.printStackTrace();
                        }
                        progressDialog.dismiss();
                    }
                }, 3000);
    }

    public boolean validateForm() {
        String email = txt_email.getText().toString();
        String password = txt_password.getText().toString();
        if (!email.matches(Regex.EMAIL.getRegex())) {
            txt_email.setError("Enter a valid email address");
            return false;
        }
        if (password.isEmpty() || password.length() < 5) {
            txt_password.setError("Enter a valid password");
            return false;
        }
        txt_password.setError(null);
        txt_email.setError(null);
        return true;
    }


    private void showMessage(String text) {
        Toast.makeText(getBaseContext(), text, Toast.LENGTH_LONG).show();
    }

}
