package org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2;

public class AuthTokenInfo {

	private String access_token;
	private String token_type;
	private String refresh_token;
	private long expires_in;
	private long expires_at;
	private String scope;

	public void setExpires_at(long expires_at) {
		this.expires_at =expires_at;
	}

	public long getExpires_at() {
		return expires_at;
	}

	public String getAccess_token() {
		return access_token;
	}

	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	public String getToken_type() {
		return token_type;
	}

	public void setToken_type(String token_type) {
		this.token_type = token_type;
	}

	public String getRefresh_token() {
		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {
		this.refresh_token = refresh_token;
	}


	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public long getExpires_in() {
		return expires_in;
	}

	public void setExpires_in(long expires_in) {
		this.expires_in = expires_in;
	}

	public boolean isExpired() {
		return (System.currentTimeMillis() >= this.getExpires_at()) ? true : false;
	}

}
