package org.shaheen.nazarov.android.sourcecodeacademy.client.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Shahin on 11/3/2016.
 */
public class MailTemplateModel implements Serializable {

    private String name;
    private String htmlText;
    private boolean htmlMode;
    private String subject;
    private Date registred = new Date();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }

    public boolean isHtmlMode() {
        return htmlMode;
    }

    public void setHtmlMode(boolean htmlMode) {
        this.htmlMode = htmlMode;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getRegistred() {
        return registred;
    }

    public void setRegistred(Date registred) {
        this.registred = registred;
    }
}