package org.shaheen.nazarov.android.sourcecodeacademy.client.model;

import java.io.Serializable;
import java.util.Set;

/**
 * Created by Shahin on 12/16/2016.
 */
public class CourseModel implements Serializable{

    private String name;
    private double amount;
    private String details;
    private boolean enabled;
    private Set<Integer> teachers;


    public Set<Integer> getTeachers() {
        return teachers;
    }

    public void setTeachers(Set<Integer> teachers) {
        this.teachers = teachers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
