package org.shaheen.nazarov.android.sourcecodeacademy.client.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Shahin on 11/3/2016.
 */
public class SubscriberModel implements Serializable {
    private int id;
    private String mail;
    private String unFollowToken;
    private boolean enabled;
    private Date created = new Date();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getUnFollowToken() {
        return unFollowToken;
    }

    public void setUnFollowToken(String unFollowToken) {
        this.unFollowToken = unFollowToken;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }



}
