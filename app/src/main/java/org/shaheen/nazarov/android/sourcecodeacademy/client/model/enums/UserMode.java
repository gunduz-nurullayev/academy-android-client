package org.shaheen.nazarov.android.sourcecodeacademy.client.model.enums;

/**
 * Created by Shahin on 10/20/2016.
 */
public enum UserMode {

    ACTIVATED,
    DEACTIVATED,
    FORGOT_PASSWORD_LINK_SEND,
    RECENTLY_PASSWORD_CHANGE;

    UserMode() {
    }
}
