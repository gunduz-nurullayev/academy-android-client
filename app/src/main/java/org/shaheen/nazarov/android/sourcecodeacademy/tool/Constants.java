package org.shaheen.nazarov.android.sourcecodeacademy.tool;

import org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2.Http;

/**
 * Created by Shahin on 1/19/2017.
 */

public interface Constants {

    String auth = "AUTH";
    String access_token = Http.ACCESS_TOKEN;
    String refresh_token = Http.REFRESH_TOKEN;
    String expired_at = "EXPIRED_AT";

    String username = Http.USERNAME;
    String password = Http.PASSWORD;

}
