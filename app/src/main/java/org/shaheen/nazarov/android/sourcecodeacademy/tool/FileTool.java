package org.shaheen.nazarov.android.sourcecodeacademy.tool;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Properties;

/**
 * Created by Shahin on 2/16/2017.
 */

public class FileTool {

    private FileInputStream fileInputStream;
    private FileOutputStream fileOutputStream;

    public FileTool(Context context) {
        try {
            fileOutputStream = context.openFileOutput("sca-settings.set" , Context.MODE_PRIVATE);
            fileInputStream = context.openFileInput("sca-settings.set");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void write(Properties properties) throws IOException {
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(properties);
        objectOutputStream.close();
    }

    public Properties read() throws IOException, ClassNotFoundException {
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        return (Properties) objectInputStream.readObject();
    }
}
