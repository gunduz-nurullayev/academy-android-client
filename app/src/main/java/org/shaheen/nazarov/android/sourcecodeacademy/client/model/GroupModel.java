package org.shaheen.nazarov.android.sourcecodeacademy.client.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by Shahin on 11/3/2016.
 */
public class GroupModel implements Serializable {


    private String name;
    private int teacher;
    private String course;
    private boolean enabled;
    private Date start;
    private Date finish;
    private Set<Long> students;

    public Set<Long> getStudents() {
        return students;
    }

    public void setStudents(Set<Long> students) {
        this.students = students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTeacher() {
        return teacher;
    }

    public void setTeacher(int teacher) {
        this.teacher = teacher;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getFinish() {
        return finish;
    }

    public void setFinish(Date finish) {
        this.finish = finish;
    }



}
