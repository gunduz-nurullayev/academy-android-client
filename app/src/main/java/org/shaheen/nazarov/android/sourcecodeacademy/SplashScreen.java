package org.shaheen.nazarov.android.sourcecodeacademy;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.shaheen.nazarov.android.sourcecodeacademy.client.HttpClient;
import org.shaheen.nazarov.android.sourcecodeacademy.client.model.UserModel;
import org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2.Http;
import org.shaheen.nazarov.android.sourcecodeacademy.tool.Constants;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class SplashScreen extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new LongOperation().execute("");
        try {
            FileOutputStream fileOutputStream = getApplicationContext().openFileOutput("sca-settings", Context.MODE_PRIVATE);
            if(fileOutputStream.){

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            HttpClient client = HttpClient.getInstance().sharedPreferences(prefs);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (!client.isNetworkAvailable((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE))) {
                return null;
            }
            try {
                UserModel userModel = client.getRequest(Http.USER+Http.INFO,UserModel.class);
                if(userModel != null){
                    return prefs.getString(Constants.username , null);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            // might want to change "executed" for the returned string passed
            // into onPostExecute() but that is upto you
            Intent intent;
            if (result != null) {
                intent = new Intent(getApplicationContext(), Home.class);
            } else {
                intent = new Intent(getApplicationContext(), LoginActivity.class);
            }
            startActivity(intent);
            finish();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }


}
