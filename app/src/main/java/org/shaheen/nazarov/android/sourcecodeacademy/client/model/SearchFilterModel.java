package org.shaheen.nazarov.android.sourcecodeacademy.client.model;


import java.io.Serializable;
import java.util.Set;

/**
 * Created by Shahin on 11/6/2016.
 */
public class SearchFilterModel implements Serializable {

    private Set<SearchModel> searchModels;
    private Set<String> orders;
    private int firstResult;
    private int maxResult;


    public Set<SearchModel> getSearchModels() {
        return searchModels;
    }

    public void setSearchModels(Set<SearchModel> searchModels) {
        this.searchModels = searchModels;
    }

    public Set<String> getOrders() {
        return orders;
    }

    public void setOrders(Set<String> orders) {
        this.orders = orders;
    }

    public int getFirstResult() {
        return firstResult;
    }

    public void setFirstResult(int firstResult) {
        this.firstResult = firstResult;
    }

    public int getMaxResult() {
        return maxResult;
    }

    public void setMaxResult(int maxResult) {
        this.maxResult = maxResult;
    }
}
