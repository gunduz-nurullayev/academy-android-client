package org.shaheen.nazarov.android.sourcecodeacademy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import org.shaheen.nazarov.android.sourcecodeacademy.client.HttpClient;
import org.shaheen.nazarov.android.sourcecodeacademy.client.model.ActivityModel;
import org.shaheen.nazarov.android.sourcecodeacademy.client.model.UserDetailsModel;
import org.shaheen.nazarov.android.sourcecodeacademy.client.model.UserModel;

import org.shaheen.nazarov.android.sourcecodeacademy.client.model.enums.ActivityMode;
import org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2.Http;
import org.shaheen.nazarov.android.sourcecodeacademy.tool.AndroidUtils;
import org.shaheen.nazarov.android.sourcecodeacademy.tool.Constants;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;


public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        new LongOperation().execute("");
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    private class LongOperation extends AsyncTask<String, Void, String[]> {

        @Override
        protected String[] doInBackground(String... params) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            HttpClient httpClient = HttpClient.getInstance().sharedPreferences(sharedPreferences);
            try {
                UserModel userModel1 = httpClient.getRequest(Http.USER + Http.INFO, UserModel.class);
                if (userModel1 == null) {
                    return null;
                }
                SimpleDateFormat dateFormat = AndroidUtils.getSimpleDateFormat();
                ActivityModel activityModel = new ActivityModel();
                activityModel.setGroup("JAVASE1");
                activityModel.setMode(ActivityMode.HERE);
                activityModel.setStudent(1L);
                activityModel.setDate(dateFormat.parse("2016-01-29 13:00:00"));
                httpClient.postRequest(Http.ACTIVITIES,activityModel);
                List<ActivityModel> activityModelList = httpClient.getRequestAsList(Http.ACTIVITIES, ActivityModel[].class);
                for (ActivityModel a : activityModelList) {
                    System.out.println(a.toString());
                }
                UserDetailsModel userDetailsModel1 = httpClient.getRequest(Http.USERDETAİLS + "/" + userModel1.getId(), UserDetailsModel.class);
                if (userDetailsModel1 != null) {
                    return new String[]{
                            userModel1.getMail(), userDetailsModel1.getFullName(), AndroidUtils.getString(userDetailsModel1.getLastAccess())};
                }




                return new String[]{
                        userModel1.getMail()};
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String[] result) {
            if (result == null) {
                return;
            }
            if (result.length > 0) {
                TextView textView = (TextView) findViewById(R.id.txt_email);
                textView.setText(result[0]);
            }
            if (result.length > 1) {
                TextView textView = (TextView) findViewById(R.id.txt_name);
                textView.setText(result[1]);
            }
            if (result.length > 2) {
                TextView textView = (TextView) findViewById(R.id.txt_lastAccess);
                textView.setText("Last Access " + result[2]);
            }
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (item.isChecked()) {
            drawer.closeDrawer(GravityCompat.START);
            return true;
        }
        Class fragmentClass = null;
        Fragment fragment = null;
        if (id == R.id.nav_home) {
            fragmentClass = CourseFragment.class;
        } else if (id == R.id.nav_activity) {
            // Handle the camera action
        } else if (id == R.id.nav_course) {
            fragmentClass = CourseFragment.class;
        } else if (id == R.id.nav_student) {

        } else if (id == R.id.nav_teacher) {

        } else if (id == R.id.nav_payment) {

        } else if (id == R.id.nav_table) {

        } else if (id == R.id.nav_settings) {

        } else if (id == R.id.nav_about) {
            fragmentClass = AboutFragment.class;
        } else if (id == R.id.nav_logout) {
            drawer.closeDrawer(GravityCompat.START);
            logout();
            return true;
        }
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_home, fragment).commit();
        }
        drawer.closeDrawer(GravityCompat.START);
        setTitle(item.getTitle());
        item.setChecked(true);
        return true;
    }

    private void logout() {
        Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String username = preferences.getString(Constants.username, null);
        preferences.edit().clear().commit();
        if (username != null)
            preferences.edit().putString(Constants.username, username);
        startActivity(intent);
        finish();
    }
}
