package org.shaheen.nazarov.android.sourcecodeacademy.client.model;


import org.shaheen.nazarov.android.sourcecodeacademy.client.model.enums.SearchAttribute;

import java.io.Serializable;

/**
 * Created by Shahin on 11/5/2016.
 */
public class SearchModel implements Serializable {

    private String name;
    private SearchAttribute attribute;
    private boolean and;
    private Object value;

    public boolean isAnd() {
        return and;
    }

    public void setAnd(boolean and) {
        this.and = and;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SearchAttribute getAttribute() {
        return attribute;
    }

    public void setAttribute(SearchAttribute attribute) {
        this.attribute = attribute;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
