package org.shaheen.nazarov.android.sourcecodeacademy.client.model;


import org.shaheen.nazarov.android.sourcecodeacademy.client.model.enums.UserMode;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Shahin on 11/3/2016.
 */
public class UserDetailsModel implements Serializable {

    private int id;
    private UserMode mode;
    private String fullName;
    private Date created;
    private Date lastAccess;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserMode getMode() {
        return mode;
    }

    public void setMode(UserMode mode) {
        this.mode = mode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(Date lastAccess) {
        this.lastAccess = lastAccess;
    }

}
