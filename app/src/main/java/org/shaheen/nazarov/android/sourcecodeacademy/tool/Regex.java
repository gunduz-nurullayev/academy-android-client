package org.shaheen.nazarov.android.sourcecodeacademy.tool;

/**
 * Created by Shahin on 3/15/2016.
 */
public enum Regex {

    EMAIL("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?","Invalid email."),
    PASSWORD("^([a-zA-Z][\\w.]+|[0-9][0-9_.]*[a-zA-Z]+[\\w.]*)$" ,"Password must be only letters , numbers , spectial characters '._' "),
    PASSWORD_FULL("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}$" ,"Minimum 8 characters at least 1 Uppercase Alphabet, 1 Lowercase Alphabet and 1 Number."),
    USERNAME("^[a-zA-Z]\\w+|[0-9][0-9_.]*[a-zA-Z]+\\w*$" , "Username must be only letters and numbers.");

    private String regex;
    private String message;

    Regex(String regex, String message) {
        this.regex = regex;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public String getRegex() {
        return regex;
    }
}
