package org.shaheen.nazarov.android.sourcecodeacademy;


import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.shaheen.nazarov.android.sourcecodeacademy.adapter.CourseAdapter;
import org.shaheen.nazarov.android.sourcecodeacademy.client.HttpClient;
import org.shaheen.nazarov.android.sourcecodeacademy.client.model.CourseModel;
import org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2.Http;

import java.io.IOException;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CourseFragment extends Fragment {

    private HttpClient client;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_course, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        client = HttpClient.getInstance();
        new CourseAsyncCall().execute("...");
    }


    public class CourseAsyncCall extends AsyncTask<String,Void,List<CourseModel>>{

        @Override
        protected List<CourseModel> doInBackground(String... params) {
            try {
                return client.getRequestAsList(Http.COURSE , CourseModel[].class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return  null;
        }

        @Override
        protected void onPostExecute(List<CourseModel> courseModels) {
            super.onPostExecute(courseModels);
            RecyclerView recList = (RecyclerView) getView().findViewById(R.id.course_recyle);
            recList.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(getContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recList.setLayoutManager(llm);
            recList.setAdapter(new CourseAdapter(courseModels));
        }
    }

}
