package org.shaheen.nazarov.android.sourcecodeacademy.client;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2.AuthTokenInfo;
import org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2.Http;
import org.shaheen.nazarov.android.sourcecodeacademy.tool.Constants;
import org.shaheen.nazarov.android.sourcecodeacademy.tool.FileTool;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Shahin on 1/22/2017.
 */

public class HttpClient {

    private static HttpClient client = new HttpClient();
    private boolean firstTime = true;

    private HttpClient() {
    }

    public static HttpClient getInstance(FileTool fileTool) {
        HttpClient
        return client;
    }

    public HttpURLConnection getConnection(String method, String url, String params, String data) throws IOException {
        URL mUrl = new URL(Http.SITE + url + "?" + (params == null ? "": params));
        HttpURLConnection httpConnection = (HttpURLConnection) mUrl.openConnection();
        httpConnection.setRequestMethod(method);
        httpConnection.setConnectTimeout(100000);
        httpConnection.setRequestProperty("Content-Type", Http.JSON_CONTENT);
        httpConnection.setRequestProperty("Accept", Http.JSON_CONTENT);
        if (url.endsWith(Http.AUTHENTICATION_SERVER_URL)) {
            final String username = "android-client";
            final String password = "nazarov19970401shaheen";
            final String basicAuth = "Basic " + Base64.encodeToString((username + ":" + password).getBytes(), Base64.NO_WRAP);
            httpConnection.setRequestProperty(Http.AUTHORIZATION, basicAuth);
        }
        if (method.equals(Http.HTTP_GET)) {
            httpConnection.setReadTimeout(100000);
            httpConnection.setAllowUserInteraction(false);
            httpConnection.setDoInput(true);
        } else if (method.equals(Http.HTTP_DELETE) || data == null || data.isEmpty()) {

        } else {
            httpConnection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(httpConnection.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();
        }
        httpConnection.connect();
        return httpConnection;
    }

    public boolean isNetworkAvailable(ConnectivityManager cm) {
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private Gson getGsonWithDateFormat() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
                .create();
        return gson;
    }

    @Nullable
    private String getAccessToken() throws IOException {
        AuthTokenInfo authTokenInfo = getAuthToken();
        if(authTokenInfo != null){
            return Http.ACCESS_TOKEN+"="+authTokenInfo.getAccess_token();
        }
        return null;
    }

    public <C> C getRequest(String url, Class<C> clazz) throws IOException {
        HttpURLConnection httpURLConnection = getConnection(Http.HTTP_GET, Http.API+url, getAccessToken(), null);
        if (httpURLConnection.getResponseCode() == Http.HTTP_UNAUTHORIZED && firstTime) {
            firstTime = false;
            return getRequest(url, clazz);
        } else if (httpURLConnection.getResponseCode() == Http.HTTP_OK) {
            firstTime = true;
            return getGsonWithDateFormat().fromJson(new InputStreamReader(httpURLConnection.getInputStream()), clazz);
        }
        firstTime = true;
        return null;
    }

    public <C> List<C> getRequestAsList(String url, Class<C[]> clazz) throws IOException {
        HttpURLConnection httpURLConnection = getConnection(Http.HTTP_GET, Http.API+url, getAccessToken(), null);
        if (httpURLConnection.getResponseCode() == Http.HTTP_UNAUTHORIZED && firstTime) {
            firstTime = false;
            return getRequestAsList(url, clazz);
        } else if (httpURLConnection.getResponseCode() == Http.HTTP_OK) {
            firstTime = true;
            return Arrays.asList(getGsonWithDateFormat().fromJson(new InputStreamReader(httpURLConnection.getInputStream()), clazz));
        }
        firstTime = true;
        return null;
    }

    public boolean postRequest(String url, Object data) throws IOException {
        String json = getGsonWithDateFormat().toJson(data);
        HttpURLConnection httpURLConnection = getConnection(Http.HTTP_POST, Http.API+url, getAccessToken(), json);
        if (httpURLConnection.getResponseCode() == Http.HTTP_UNAUTHORIZED && firstTime) {
            firstTime = false;
            return postRequest(url, data);
        } else if (httpURLConnection.getResponseCode() == Http.HTTP_CREATED || httpURLConnection.getResponseCode() == Http.HTTP_OK) {
            firstTime = true;
            return true;
        }
        firstTime = true;
        return false;
    }

    public <C> List<C> postRequestAsList(String url, Object data) throws IOException {
        String json = getGsonWithDateFormat().toJson(data);
        HttpURLConnection httpURLConnection = getConnection(Http.HTTP_POST, Http.API+url, getAccessToken(), json);
        if (httpURLConnection.getResponseCode() == Http.HTTP_UNAUTHORIZED && firstTime) {
            firstTime = false;
            return postRequestAsList(url, data);
        } else if (httpURLConnection.getResponseCode() == Http.HTTP_CREATED || httpURLConnection.getResponseCode() == Http.HTTP_OK) {
            Type listType = new TypeToken<List<C>>() {
            }.getType();
            firstTime = true;
            return getGsonWithDateFormat().fromJson(new InputStreamReader(httpURLConnection.getInputStream()), listType);
        }
        firstTime = true;
        return null;
    }

    public AuthTokenInfo getAuthToken() throws IOException {
        String username = properties.getProperty(Constants.username,null);
        String refreshToken = properties.getProperty(Constants.refresh_token, null);
        if (Boolean.parseBoolean(properties.getProperty(Constants.auth, "true"))) {
            long expiredAt = Long.parseLong(properties.getProperty(Constants.expired_at, "0L"));
            if (expiredAt > System.currentTimeMillis() + 500) {
                String access_token = properties.getProperty(Constants.access_token, null);
                AuthTokenInfo authTokenInfo = new AuthTokenInfo();
                authTokenInfo.setExpires_at(expiredAt);
                authTokenInfo.setAccess_token(access_token);
                authTokenInfo.setRefresh_token(refreshToken);
                return authTokenInfo;
            }
        }
        if(refreshToken != null) {
            String grantType = Http.REFRESH_TOKEN;
            String params = String.format("%s=%s&%s=%s", Http.GRANT_TYPE, grantType, Http.REFRESH_TOKEN, refreshToken);
            HttpURLConnection httpURLConnection = getConnection(Http.HTTP_POST, Http.AUTHENTICATION_SERVER_URL, params, null);
            if(firstTime && httpURLConnection.getResponseCode() == Http.HTTP_UNAUTHORIZED){
                firstTime = false;
                return getAuthToken();
            }else if(httpURLConnection.getResponseCode() == Http.HTTP_BAD_REQUEST){

            }
            else if (httpURLConnection.getResponseCode() == Http.HTTP_OK) {
                AuthTokenInfo authTokenInfo = getGsonWithDateFormat()
                        .fromJson(new InputStreamReader(httpURLConnection.getInputStream()),AuthTokenInfo.class);
                authTokenInfo.setExpires_at(System.currentTimeMillis() + authTokenInfo.getExpires_in() * 1000);
                prefs.edit()
                        .putString(Constants.refresh_token, authTokenInfo.getRefresh_token())
                        .putString(Constants.access_token, authTokenInfo.getAccess_token())
                        .putLong(Constants.expired_at, authTokenInfo.getExpires_at())
                        .commit();
                firstTime=true;
                return authTokenInfo;
            }
        }
        String password = properties.getProperty(Constants.password, null);
        if (password == null || username == null) {
            return null;
        }

        try {
            String grantType = Http.PASSWORD;
            String params = String.format("%s=%s&%s=%s&%s=%s", Http.GRANT_TYPE, grantType, Http.PASSWORD, password,Http.USERNAME,username);
            HttpURLConnection httpURLConnection = getConnection(Http.HTTP_POST, Http.AUTHENTICATION_SERVER_URL, params, null);
            if(firstTime && httpURLConnection.getResponseCode() == Http.HTTP_UNAUTHORIZED){
                firstTime = false;
                return getAuthToken();
            }else if (httpURLConnection.getResponseCode() == Http.HTTP_OK) {
                AuthTokenInfo authTokenInfo = getGsonWithDateFormat()
                        .fromJson(new InputStreamReader(httpURLConnection.getInputStream()),AuthTokenInfo.class);
                authTokenInfo.setExpires_at(System.currentTimeMillis() + authTokenInfo.getExpires_in() * 1000);
                prefs.edit()
                        .putString(Constants.refresh_token, authTokenInfo.getRefresh_token())
                        .putString(Constants.access_token, authTokenInfo.getAccess_token())
                        .putLong(Constants.expired_at, authTokenInfo.getExpires_at())
                        .commit();
                firstTime=true;
                return authTokenInfo;
            }
        } catch (IOException e) {
            e.printStackTrace();

        }
        firstTime=true;
        return null;
    }

    public boolean putRequest(String url, Object data) throws IOException {
        String json = getGsonWithDateFormat().toJson(data);
        HttpURLConnection httpURLConnection = getConnection(Http.HTTP_PUT, Http.API+url, getAccessToken(), json);
        if (httpURLConnection.getResponseCode() == Http.HTTP_UNAUTHORIZED && firstTime) {
            firstTime = false;
            return putRequest(url, data);
        } else if (httpURLConnection.getResponseCode() == Http.HTTP_CREATED || httpURLConnection.getResponseCode() == Http.HTTP_OK) {
            firstTime = true;
            return true;
        }
        firstTime = true;
        return false;
    }

    public boolean deleteRequest(String url) throws IOException {
        HttpURLConnection httpURLConnection = getConnection(Http.HTTP_DELETE, Http.API+url, getAccessToken(), null);
        if (httpURLConnection.getResponseCode() == Http.HTTP_UNAUTHORIZED && firstTime) {
            firstTime = false;
            return deleteRequest(url);
        } else if (httpURLConnection.getResponseCode() == Http.HTTP_CREATED || httpURLConnection.getResponseCode() == Http.HTTP_OK) {
            firstTime = true;
            return true;
        }
        firstTime = true;
        return false;
    }

}
