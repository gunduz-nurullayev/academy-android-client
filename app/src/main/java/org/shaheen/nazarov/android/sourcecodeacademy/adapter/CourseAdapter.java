package org.shaheen.nazarov.android.sourcecodeacademy.adapter;

import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.shaheen.nazarov.android.sourcecodeacademy.R;
import org.shaheen.nazarov.android.sourcecodeacademy.client.model.CourseModel;

import java.util.List;

/**
 * Created by Shahin on 1/23/2017.
 */

public class CourseAdapter extends RecyclerView.Adapter<CourseAdapter.CardViewHolder> {

    private List<CourseModel> courseModels;

    public CourseAdapter(List<CourseModel> courseModels) {
        this.courseModels = courseModels;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.
                from(parent.getContext()).inflate(R.layout.card_view_course, parent, false);
        CardViewHolder viewHolder = new CardViewHolder(linearLayout);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        CourseModel courseModel = courseModels.get(position);
        if(courseModel != null){
            holder.name.setText(courseModel.getName());
            if(courseModel.isEnabled()){
                holder.enabled.setTextColor(Color.GREEN);
                holder.enabled.setText("Enabled");
            }else{
                holder.enabled.setTextColor(Color.RED);
                holder.enabled.setText("Disabled");
            }
            holder.amount.setText(Math.round(courseModel.getAmount()) + " AZN");
            holder.teacherCount.setText("Teachers Count :" + courseModel.getTeachers().size());
            holder.details.setText(courseModel.getDetails());
        }
    }

    @Override
    public int getItemCount() {
        return courseModels.size();
    }


    public static class CardViewHolder extends RecyclerView.ViewHolder {

        protected CheckBox name;
        protected TextView enabled;
        protected TextView amount;
        protected TextView details;
        protected TextView teacherCount;

        public CardViewHolder(View itemView) {
            super(itemView);
            name = (CheckBox) itemView.findViewById(R.id.course_chk);
            enabled = (TextView) itemView.findViewById(R.id.course_enable);
            amount = (TextView) itemView.findViewById(R.id.course_amount);
            details = (TextView) itemView.findViewById(R.id.course_details);
            teacherCount = (TextView) itemView.findViewById(R.id.teacher_count);
        }




    }
}
