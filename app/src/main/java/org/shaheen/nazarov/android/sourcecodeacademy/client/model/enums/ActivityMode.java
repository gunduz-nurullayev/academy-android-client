package org.shaheen.nazarov.android.sourcecodeacademy.client.model.enums;

/**
 * Created by Shahin on 12/17/2016.
 */
public enum ActivityMode {

    EXCUSED,
    HERE,
    NOT_HERE,
    LESSON_CANCELED;

    ActivityMode() {}

}
