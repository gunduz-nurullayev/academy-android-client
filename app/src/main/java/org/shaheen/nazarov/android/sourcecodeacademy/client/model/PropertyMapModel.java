package org.shaheen.nazarov.android.sourcecodeacademy.client.model;

import java.io.Serializable;

/**
 * Created by Shahin on 1/4/2017.
 */
public class PropertyMapModel  implements Serializable {

    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


}
