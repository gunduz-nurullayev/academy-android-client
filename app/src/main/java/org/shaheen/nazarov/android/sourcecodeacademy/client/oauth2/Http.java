package org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2;


public interface Http {



    String API = "/api";

    String PUBLIC = "/public";
    String PUBLIC_RECOVER = PUBLIC + "/recover";
    String PUBLIC_FORGOT = PUBLIC + "/forgot";
    String PUBLIC_SUBSCRIBE = PUBLIC + "/subscribe";
    String PUBLIC_UNSUBSCRIBE =PUBLIC + "/unsubscribe";
    String PUBLIC_FEEDBACK = PUBLIC + "/feedback";


    String USER = "/users";
    String GROUP = "/groups";
    String ACTIVITIES = "/activities";
    String COURSE = "/courses";
    String STUDENT = "/students";
    String TEACHER = "/teachers";
    String LESSON = "/lessons";
    String PAYMENT = "/payments";
    String TABLE = "/tables";
    String USERDETAİLS = "/user/details";
    String MAILTEMPLATE = "/mail/templates";
    String FEEDBACK = "/feedbacks";
    String PROPERTYMAP = "/properties";
    String SUBSCRIBER = "/subscribers";


    String INFO = "/info";

//    String SITE = "http://localhost:8080";
    String SITE = "http://api-nazarov.rhcloud.com";
    String ACCESS_TOKEN = "access_token";
//    String CLIENT_ID = "client_id";
//    String CLIENT_SECRET = "client_secret";
    String REFRESH_TOKEN = "refresh_token";
    String USERNAME = "username";
    String PASSWORD = "password";
    String AUTHENTICATION_SERVER_URL = "/oauth/token";
    String RESOURCE_SERVER_URL = SITE + API;
    String GRANT_TYPE = "grant_type";
//    String SCOPE = "scope";
    String FILTER = "/filter";
    String AUTHORIZATION = "Authorization";
//    String BEARER = "Bearer";
//    String BASIC = "Basic";
    String JSON_CONTENT = "application/json; charset=utf-8";
    String XML_CONTENT = "application/xml; charset=utf-8";
    String URL_ENCODED_CONTENT = "application/x-www-form-urlencoded; charset=utf-8";
//    String EXPIRES_IN = "expires_in";
//    String TOKEN_TYPE = "token_type";

    int HTTP_OK = 200;
    int HTTP_CREATED = 201;
    int HTTP_NO_CONTENT = 204;
    int HTTP_BAD_REQUEST = 400;
    int HTTP_FORBIDDEN = 403;
    int HTTP_NOT_FOUND = 404;
    int HTTP_UNAUTHORIZED = 401;

    String HTTP_GET = "GET";
    String HTTP_POST = "POST";
    String HTTP_PUT = "PUT";
    String HTTP_DELETE = "DELETE";

}
