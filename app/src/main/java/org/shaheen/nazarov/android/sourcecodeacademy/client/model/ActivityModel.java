package org.shaheen.nazarov.android.sourcecodeacademy.client.model;


import org.shaheen.nazarov.android.sourcecodeacademy.client.model.enums.ActivityMode;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Shahin on 12/17/2016.
 */
public class ActivityModel implements Serializable {

    private long id;
    private String group;
    private long student;
    private ActivityMode mode;
    private Date date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public long getStudent() {
        return student;
    }

    public void setStudent(long student) {
        this.student = student;
    }

    public ActivityMode getMode() {
        return mode;
    }

    public void setMode(ActivityMode mode) {
        this.mode = mode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }


    @Override
    public String toString() {
        return "ActivityModel{" +
                "id=" + id +
                ", group='" + group + '\'' +
                ", student=" + student +
                ", mode=" + mode +
                ", date=" + date +
                '}';
    }
}
