package org.shaheen.nazarov.android.sourcecodeacademy;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.test.ApplicationTestCase;
import android.util.Log;

import org.shaheen.nazarov.android.sourcecodeacademy.client.HttpClient;
import org.shaheen.nazarov.android.sourcecodeacademy.client.model.ActivityModel;
import org.shaheen.nazarov.android.sourcecodeacademy.client.model.enums.ActivityMode;
import org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2.AuthTokenInfo;
import org.shaheen.nazarov.android.sourcecodeacademy.client.oauth2.Http;
import org.shaheen.nazarov.android.sourcecodeacademy.tool.AndroidUtils;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    public void test() throws Exception {
        HttpClient client = HttpClient.getInstance();
//        SimpleDateFormat dateFormat = AndroidUtils.getSimpleDateFormat();
//        dateFormat.parse("2016-");
//        ActivityModel activityModel = new ActivityModel();
//        activityModel.setGroup("JAVA-SE");
//        activityModel.setMode(ActivityMode.HERE);
//        activityModel.setStudent(1L);
//        activityModel.setDate();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        client.sharedPreferences(sharedPreferences);
        List<ActivityModel> activityModelList = client.getRequestAsList(Http.ACTIVITIES,ActivityModel[].class);
        for(ActivityModel a : activityModelList){
            System.err.println(a.toString());

        }
    }
}